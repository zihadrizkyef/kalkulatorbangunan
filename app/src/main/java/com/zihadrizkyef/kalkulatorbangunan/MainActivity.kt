package com.zihadrizkyef.kalkulatorbangunan

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.EditText
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //Get data
        //validasi
        //result data

        bt_hitung.setOnClickListener {
            val isValid = formIsValid()
            if (isValid) {
                hitung()
            }
        }
    }

    private fun formIsValid(): Boolean {
        if (et_panjang.text.toString().isBlank()) {
            Toast.makeText(this, "Mohon isi panjang", Toast.LENGTH_LONG).show()
            return false
        }

        if (et_lebar.text.toString().isBlank()) {
            Toast.makeText(this, "Mohon isi lebar", Toast.LENGTH_LONG).show()
            return false
        }

        if (et_tinggi.text.toString().isBlank()) {
            Toast.makeText(this, "Mohon isi tinggi", Toast.LENGTH_LONG).show()
            return false
        }

        return true
    }

    private fun hitung() {
        val panjang = et_panjang.text.toString().toInt()
        val lebar = et_lebar.text.toString().toInt()
        val tinggi = et_tinggi.text.toString().toInt()
        tv_hasil.text = "Hasil : ${panjang*lebar*tinggi}"
    }
}
